/* 
   Name: fan_control.c
	 Author: Zachary Ediger
*/

#include "stm32f10x.h"
#include "fan_control.h"
#include "clocks_lib.h"
 
 void a2d_init (void)
{
	//	A/D system initialize for throttle
		RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
		ADC1->CR2 |= ADC1_CR2_ON;
		ADC1->SMPR2 |= ADC1_SMPR2_SAMPLERATE;
		ADC1->SQR3=12;
	  ADC1->CR1 = ADC_CR1_EOCIE;					//enable "end of conversion interupts"
		delay(8000);
}

 void pwmInit(void)
 {
	 // Initializes PWM registers
	 TIM2->CR1= TIM_CR1_CEN;
	 TIM2->CR2= TIM_CR2_OIS2;
	 TIM2->EGR= TIM_EGR_UG;
	 TIM2->CCMR1|= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE | TIM_CCMR1_OC2FE;  //(enables ghost register)
	 TIM2->CCER= TIM_CCER_CC2E;
	 TIM2->PSC=2399; // 1s/0.1ms=10000   (24Mhz/10000)-1=2399
	 TIM2->ARR=100; // for 10ms
	 TIM2->CCR2=0; // for 0% duty cycle
	 TIM2->BDTR|= TIM_BDTR_MOE | TIM_BDTR_OSSI;
	 TIM2->CR1|= TIM_CR1_ARPE | TIM_CR1_CEN;
 }
 
 
 
 