/******************************************************************************
 * Name:    SPI_lib.h
 * Description: STM32 spi initialization
 * Version: V1.00
 * Authors: Roger Boutin.  
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>



 //Functions available for public use
 
 void SPIInit(void); // Initialize the spi functionality
 void WriteSPICommand(uint8_t data); //send a command to the SPI bus
 int ReadSPI(void);//read the spi data on SPI2
