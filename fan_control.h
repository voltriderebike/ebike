/* 
   Name: fan_control.h
	 Author: Zachary Ediger
*/
#include <stdint.h>

#define GREEN_ON	0X00000200 
#define GREEN_OFF	0x02000000 
#define BLUE_ON	0x00000100 
#define BLUE_OFF	0x01000000 

#define ADC1_CR2_ON 0x5
#define ADC1_SMPR2_SAMPLERATE 0xFFF 
#define ADC1_SQR3_CHANNELSEQUENCE 0x00 
#define ADC1_CR2_STARTSEQUENCE 0x1 


 //Functions available for public use
 
 void pwmInit(void);
 void a2d_init (void);

