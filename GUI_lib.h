/******************************************************************************
 * Name:    BUI_lib.h
 * Description: STM32 peripherals initialization
 * Version: V1.00
 * Authors: Roger Boutin.  
 *
 * 
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>



 //Functions available for public use
void RunGUI(uint8_t btnRight, uint8_t btnLeft, uint8_t btnUp, uint8_t btnDown, uint8_t btnEnter);
void MainMenu(uint8_t btnUp, uint8_t btnDown, uint8_t btnEnter);
void UpdateSpeed(void);

void MainScreen(void);
void MainSize(void);
void MainSensor(void);
void MainLimit(void);

void SizeSetup(void);
void SensorSetup(void);
void LimiterSetup(void);

void WheelSize(uint8_t btnRight, uint8_t btnLeft, uint8_t btnUp, uint8_t btnDown, uint8_t btnEnter);
void WheelSensor(uint8_t btnRight, uint8_t btnLeft, uint8_t btnUp, uint8_t btnDown, uint8_t btnEnter);
void SpeedLimit(uint8_t btnRight, uint8_t btnLeft, uint8_t btnEnter);
void Saved(void);
