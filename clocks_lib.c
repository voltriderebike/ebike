/* 
   Name: clocks_lib.c
	 Author: Zachary Ediger
*/
 
 #include "stm32f10x.h"
 #include "clocks_lib.h"
  
// CLOCK AND TIMING FUNCTIONS 

/*
* Name:					void clockInit()
* Paramaters: 	none
* Description: 	This function will initialize the device internal clock to 24 Mhz
*/
void clockInit()
{
	uint32_t temp = 0x00;
	RCC->CFGR = 0x00050002;	
	//RCC->CFGR = 0x07050002;		// Output PLL/2 as MCO, PLLMUL X3, PREDIV1 is PLL input (Roger's code)
	RCC->CR =  0x01010081;	    // Turn on PLL, HSE, HSI
	
	while (temp != 0x02000000)  // Wait for the PLL to stabilize                           
	{
				temp = RCC->CR & 0x02000000; //Check to see if the PLL lock bit is set
	}
	
	//Enable peripheral clocks for various ports and subsystems
	  RCC->APB1ENR |= RCC_APB1ENR_TIM2EN; 	//TIM2 Enable
	
		RCC->APB2ENR |= RCC_APB2ENR_AFIOEN | RCC_APB2ENR_TIM1EN;
		RCC->APB2ENR |=  RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN; 
	  
	// Set configuration and mode bits for PA1 (pwm output)
	  GPIOA->CRL |= GPIO_CRL_CNF1_1 | GPIO_CRL_MODE1_1 | GPIO_CRL_MODE1_0 ;
		GPIOA->CRL &= ~GPIO_CRL_CNF1_0 ;	
	
		GPIOC->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_MODE0; // Set configuration and mode bits for PC0 (front sensor input)
		GPIOC->CRL &= ~GPIO_CRL_CNF1 & ~GPIO_CRL_MODE0; // Set configuration and mode bits for PC1 (rear sensor input)
		GPIOC->CRL &= ~GPIO_CRL_CNF2 & ~GPIO_CRL_MODE0; // Set configuration and mode bits for PC2 (throttle input)
	  
	// Set more options for TIM1
	 TIM1->CR1= TIM_CR1_CEN;
	 TIM1->CR2= TIM_CR2_OIS1;
	 TIM1->EGR= TIM_EGR_UG;
	 TIM1->CCMR1|= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1PE | TIM_CCMR1_OC1FE;  //(enables ghost register)
	 TIM1->CCER= TIM_CCER_CC1E;
	 TIM1->PSC=2399; // 1s/0.1ms=10000   (24Mhz/10000)-1=2399
	 TIM1->ARR=60000; // for 6000ms (or 6 seconds)
	 TIM1->CCR1=0; // for 50% duty cycle
	 TIM1->BDTR|= TIM_BDTR_MOE | TIM_BDTR_OSSI;
	 TIM1->CR1|= TIM_CR1_ARPE | TIM_CR1_CEN;
	
	
	//Enable LEDs (PC8, PC9)
		GPIOC->CRH |= GPIO_CRH_MODE9 | GPIO_CRH_MODE8 ;
		GPIOC->CRH &= ~GPIO_CRH_CNF9 & ~GPIO_CRH_CNF8 ;
}

void delay(uint32_t delay)
{
  int i=0;
	for(i=0; i< delay; ++i)  //a value of 6000 gives approximately 1 mS of delay)
	{
	}
	
}

