/******************************************************************************
 * Name:    SPI_lib.c
 * Description: STM32 spi initialization and functions
 * Author: Roger Boutin.  
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *			
 *****************************************************************************/
 #include "stm32f10x.h"
 #include "SPI_lib.h"
 //#include "timing_lib.h"
 #include "clocks_lib.h"
	
	

/*
* Name:					void SPIInit()
* Paramaters: 	none
* Description: 	This function will initialize the spi functions
*/
void SPIInit()
{
	//enable clocks
		RCC->APB2ENR |=  RCC_APB2ENR_AFIOEN | RCC_APB2ENR_IOPAEN| RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_SPI1EN;
		RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;

	// set PA7 as alternate function push pull	output - mosi - to shift register pin 14
		GPIOA->CRL |= GPIO_CRL_CNF7_1 | GPIO_CRL_MODE7 ;
		GPIOA->CRL &= ~GPIO_CRL_CNF7_0 ;

	// set PA5 as alternate function push pull	output - sck - to shift register pin 11
		GPIOA->CRL |= GPIO_CRL_CNF5_1 | GPIO_CRL_MODE5;
		GPIOA->CRL &= ~GPIO_CRL_CNF5_0 ;

	// set PB14 as floating input	- miso from button shift register
		GPIOB->CRH |= GPIO_CRH_CNF14_0 ;
		GPIOB->CRH &= ~GPIO_CRH_MODE14 & ~GPIO_CRH_CNF14_1;
	
	// set PB13 as alternate function push pull output - clk for button shift register
		GPIOB->CRH |= GPIO_CRH_CNF13_1 | GPIO_CRH_MODE13 ;
		GPIOB->CRH &= ~GPIO_CRH_CNF13_0 ;
	
	// set PC12 as general purpose push pull output - latch for button shift register
	//change this to a different pin later on.
		GPIOC->CRH |= GPIO_CRH_MODE12 ;
		GPIOC->CRH &= ~GPIO_CRH_CNF12 ;

		
		//config SPI1
		SPI1->CR2 = 0x00000000;
		//SPI1->CR1 =  0x0000032C;
		SPI1->CR1 =  0x0000033C;
		SPI1->CR1 |= 0x40;
		
		//config SPI2
		SPI2->CR2 = 0x00000000;
		SPI2->CR1 =  0x0000032C;
		SPI2->CR1 |= 0x40;
}


/*
* Name:					int WriteSPICommand(uint8_t)
* Paramaters: 	data - data to be sent over the SPI link
* Returns: nothing
* Description: 	This function will send a 7-bit command over the SPI bus
*/
void WriteSPICommand(uint8_t data)
{
	int readData=0;
	
	//a quirk with the 74hc595 requires an extra clock cycle to send
	//all 8 data bits.  I can't figure out how to send an extra clock cycle 
	//without changing the pin to a gpio and back, so it will only send the
	//bottom 7 bits.  shift left so a garbage bit is lost.
	
	data = data<<1;

	while( (SPI1->SR & SPI_SR_TXE) !=0x2)
	{
	}
	SPI1->DR = data;	//send data
	
	while( (SPI1->SR & SPI_SR_RXNE) !=1)
	{
	}
	
	//read data to clear txe flag
	readData=SPI1->DR;
}


/*
* Name:					int ReadSPI()
* Paramaters: 	none
* Returns: The data on the SPI2->DR register
* Description: 	This function will send a command over the SPI bus
*/
int ReadSPI()
{
	int readData=0;

	//toggle pc12(PL) to enable parallel data read
	GPIOC->BSRR |= GPIO_BSRR_BS12;
	delay(100);
	GPIOC->BSRR |= GPIO_BSRR_BR12;
	
	while( (SPI2->SR & SPI_SR_TXE) !=0x2)
	{
	}
	SPI2->DR = 0x99;	//load garbage data in order to read buttons
	
	while( (SPI2->SR & SPI_SR_RXNE) !=1)
	{
	}
	
	readData=SPI2->DR;
	
	return readData;
}
