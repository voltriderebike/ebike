/* 
   Name: clocks_lib.h
	 Author: Zachary Ediger
*/

#include <stdint.h>


 //Functions available for public use
 
 void clockInit(void); // Initialize the Cortex M3 clock using the RCC registers
 void SysTickInit(void);  //Initialize interrupts
 void delay(uint32_t delay); // A general purpose countdown timer delay routine
 