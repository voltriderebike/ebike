/******************************************************************************
 * Name:    LCD_lib.c
 * Description: STM32 peripherals initialization and functions
 * Version: V1.00
 * Author: Roger Boutin.  
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
 #include "stm32f10x.h"
 #include "LCD_lib.h"
// #include "timing_lib.h"
 #include "clocks_lib.h"
 #include "SPI_lib.h"

	
//Commands for Hitachi 44780 compatible LCD controllers
#define LCD_8B2L	0x38	//; Enable 8 bit data, 2 display lines
#define LCD_4B2L	0x28	//; Enable 4 bit data, 2 display lines
#define LCD_DCB		0x0F	//; Enable Display, Cursor, Blink
#define LCD_MCR		0x06	//; Set Move Cursor Right
#define LCD_CLR		0x01	//; Home and clear LCD
#define LCD_LN1		0x80	//;Set DDRAM to start of line 1
#define LCD_LN2		0xC0	//; Set DDRAM to start of line 2

// Control signal manipulation for LCDs on 352/384/387 board
//PC6:EN PA7:DS PA5:CLK

	
/*
* Name:LCDInit
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:initializes pins for LCD and LCD controller
*/
void LCDInit (void)
{
	//Set the config and mode bit for Port C bits 6 so it will
	// be a push-pull output (up to 50 MHz)
		GPIOC->CRL |= GPIO_CRL_MODE6;
		GPIOC->CRL &= ~GPIO_CRL_CNF6;

		//LCD initialization function
		
		//wakeup sequence
		delay(90000);//wait 15 ms
		WriteNibble(0x3, 0);
		delay(30000);	//wait 5ms
		WriteNibble(0x3, 0);
		delay(6000); // wait 1ms
		WriteNibble(0x3, 0);
		delay(30000);	//wait 5ms
		WriteNibble(0x2, 0);
		delay(30000);	//wait 5ms
	
		commandToLCD(LCD_4B2L);//set 4-bit mode
		commandToLCD(LCD_DCB);//Enable Display, Cursor, Blink
		commandToLCD(LCD_MCR);//Set Move Cursor Right
		commandToLCD(LCD_CLR);//Home and clear LCD
}

/*
* Name:WriteNibble
* Type:PUBLIC
* Parameters:a single nibble of information for the LCD controller
* Returns:nothing
* Description:This function generates control timing and data signals to send one nibble to the LCD 
* Caller needs to add extra delay for clear display or return home commands
* 
*/
void WriteNibble(uint8_t nibble, uint8_t RS)
{
	//move nibble to bits 4-7
	//nibble = nibble<<4;
	//nibble = nibble&0xF0;

	//format data to send
	//bit 4 is RS, 3-0 is nibble
	nibble = nibble&0x0f;
	RS = RS&0x01;
	RS = RS<<4;
	nibble = nibble|RS;
	
	//write data sequence - en high, send data, en low
	GPIOC->BSRR = GPIO_BSRR_BS6;
	WriteSPICommand(nibble);
	GPIOC->BSRR = GPIO_BSRR_BR6;
	
	delay(1000);//170us
}

/*
* Name:commandToLCD
* Type:PUBLIC
* Parameters:a single byte of command information for the LCD controller
* Returns:nothing
* Description:This function seperates a command byte into two nibbles to be written to the LCD
*/
void commandToLCD(uint8_t data)
{
	uint8_t lowNibble, highNibble;
	lowNibble = data & 0x0F;
	highNibble = data>>4;
	WriteNibble(highNibble, 0);
	WriteNibble(lowNibble, 0);
	
	//add extra delay for clear display or return home
	if ((data ==0x01)||(data==0x02)||(data==0x03))
			delay(8200);//1.4ms
}

/*
* Name:DataToLCD
* Type:PUBLIC
* Parameters:a single byte of data for the LCD controller
* Returns:nothing
* Description:This function seperates a data byte into two nibbles to be written to the LCD
*/
void DataToLCD(uint8_t data)
{
	uint8_t lowNibble, highNibble;
	lowNibble = data & 0x0F;
	highNibble = data>>4;
	WriteNibble(highNibble, 1);
	WriteNibble(lowNibble, 1);	
}

/*
* Name:WriteString
* Type:PUBLIC
* Parameters:a character string to be written to the lcd
* Returns:nothing
* Description:This function sends the string to the lcd one character at a time
*/
void WriteString(char * data)
{
	int i=0;
	while(data[i]!='\0')
	{
		DataToLCD(data[i]);
		i++;
	}
}

