/******************************************************************************
 * Name:    LCD_lib.c
 * Description: STM32 peripherals initialization and functions
 * Author: Roger Boutin.  
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 *****************************************************************************/
 #include "stm32f10x.h"
 #include "LCD_lib.h"
 #include "GUI_lib.h"
 #include "stdio.h"//for sprintf - writing variables to the screen
 //#include "timing_lib.h
 #include "clocks_lib.h"
 #include "eeprom.h"

	//Commands for Hitachi 44780 compatible LCD controllers
#define LCD_CLR		0x01	//; Home and clear LCD
#define LCD_LN1		0x80	//;Set DDRAM to start of line 1
#define LCD_LN2		0xC0	//; Set DDRAM to start of line 2
#define LCD_HID		0xD0	//; Set DDRAM to offscreen (end of line 2)
#define LCD_LFT		0x7F	//; code for left arrow
#define LCD_RGT		0x7E	//; code for right arrow

//testing for flash write
#define CR_PG_Set                ((uint32_t)0x00000001)
#define CR_PG_Reset              ((uint32_t)0x00001FFE) 

//globals
int state = 1;
uint16_t wheelSize = 45;
uint16_t magnets = 4;
uint16_t speedLimit = 1;
uint8_t currentSpeed = 32;//need to make external for data from zach
uint8_t currentBattery = 32;//need to make external for data from zach

extern uint16_t VirtAddVarTab[NumbOfVar];

/*
* Name:RunGUI
* Type:PUBLIC
* Parameters:5 button inputs
* Returns:nothing
* Description:This function Runs the GUI system
*/
void RunGUI(uint8_t btnRight, uint8_t btnLeft, uint8_t btnUp, uint8_t btnDown, uint8_t btnEnter)
{
	//enter the main or sub menu depending on state
	if (state<10)
			MainMenu(btnUp, btnDown, btnEnter);
	else if (state < 30)
		WheelSize(btnRight, btnLeft, btnUp, btnDown, btnEnter);
	else if (state < 40)
		WheelSensor(btnRight, btnLeft, btnUp, btnDown, btnEnter);
	else if (state < 50)
		SpeedLimit(btnRight, btnLeft, btnEnter);
	
}

/*
* Name:MainMenu
* Type:PUBLIC
* Parameters: btnUp, btnDown, btnEnter
* Returns:nothing
* Description:This function runs the main menu and handles the states
*/
void MainMenu(uint8_t btnUp, uint8_t btnDown, uint8_t btnEnter)
{
	//first, find out what state we need to go to
		if (state == 1)//main menu
		{
			if (btnDown!=0)
				state = 2;
			if (btnUp!=0)
				state = 4;
		}
		else if (state == 2)//Wheel Size Setup
		{
			if (btnDown!=0)
				state = 3;
			if (btnUp!=0)
				state = 1;
			if (btnEnter!=0)
				state = 20;
		}
		else if (state == 3)//Wheel Sensor Setup
		{
			if (btnDown!=0)
				state = 4;
			if (btnUp!=0)
				state = 2;
			if (btnEnter!=0)
				state = 30;
		}
		else if (state == 4)//Speed Limiter
		{
			if (btnDown!=0)
				state = 1;
			if (btnUp!=0)
				state = 3;
			if (btnEnter!=0)
				state = 40;
		}
	
		
		//now the current state is set, write the screen for the new state
		if (state == 1)
			MainScreen();
		else if (state == 2)
				MainSize();
		else if (state == 3)
			MainSensor();
		else if (state == 4)
			MainLimit();
		else if (state==20)
			SizeSetup();
		else if (state==30)
			SensorSetup();
		else if (state==40)
			LimiterSetup();
}

/*
* Name:UpdateSpeed
* Type:PUBLIC
* Parameters: nothing
* Returns:nothing
* Description:This function updates the speed on the screen
*/
void UpdateSpeed(void)
{
		char str[3];
		if (state!=1)//only update speed on main menu
			return;
		commandToLCD(LCD_LN1+7);
		sprintf(str,"%02d", currentSpeed);
		WriteString(str);
		commandToLCD(LCD_HID);
}

/*
* Name:UpdateBattery
* Type:PUBLIC
* Parameters: nothing
* Returns:nothing
* Description:This function updates the speed on the screen
*/
void UpdateBattery(void)
{
		char str[3];
		if (state!=1)//only update battery on main menu
			return;
		commandToLCD(LCD_LN1+7);
		if (currentBattery == 100)
			sprintf(str,"%03d", currentBattery);
		else
			sprintf(str,"%02d", currentBattery);
		WriteString(str);
		commandToLCD(LCD_HID);
}

/*
* Name:WheelSize
* Type:PUBLIC
* Parameters: 5 button input
* Returns:nothing
* Description:This function runs the Wheel Size sub menu
*/
void WheelSize(uint8_t btnRight, uint8_t btnLeft, uint8_t btnUp, uint8_t btnDown, uint8_t btnEnter)
{
	char str[3];
	
	//if we're saving, we don't need to do any other code
	if (btnEnter!=0)
	{
		//write to memory
		EE_WriteVariable(VirtAddVarTab[1], wheelSize);
		Saved();
	}
	
	//tens position
	if (state == 21)
	{
		if (btnRight !=0)
		{
			state = 22;
			commandToLCD(LCD_LN2+1);
			return;
		}
		else if (btnDown!=0)
		{
			if(wheelSize<10)
				wheelSize = wheelSize + 90;
			else
				wheelSize = wheelSize-10;
		}
		else if (btnUp!=0)
		{
			if(wheelSize>90)
				wheelSize = wheelSize - 90;
			else
				wheelSize = wheelSize+10;
		}
		//get current value and write it
		commandToLCD(LCD_LN2);
		sprintf(str,"%02d", wheelSize);
		WriteString(str);
		commandToLCD(LCD_LN2);
	}
		
	//ones position
	else if (state == 22)
	{
		if (btnLeft !=0)
		{
			state = 21;
			commandToLCD(LCD_LN2);
			return;
		}
		else if (btnDown!=0)
		{
			if ((wheelSize%10) ==0)
				wheelSize = wheelSize + 9;
			else
				wheelSize = wheelSize-1;
		}
		else if (btnUp!=0)
		{
			if((wheelSize%10) ==9)
				wheelSize = wheelSize - 9;
			else
				wheelSize = wheelSize+1                                                                    ;
		}
		//get current value and write it
		commandToLCD(LCD_LN2);
		sprintf(str,"%02d", wheelSize);
		WriteString(str);
		commandToLCD(LCD_LN2+1);
	}
	
}

/*
* Name:WheelSensor
* Type:PUBLIC
* Parameters:5 button input
* Returns:nothing
* Description:This function runs the Wheel Size sub menu
*/
void WheelSensor(uint8_t btnRight, uint8_t btnLeft, uint8_t btnUp, uint8_t btnDown, uint8_t btnEnter)
{
	char str[3];
	
	//if we're saving, we don't need to do any other code
	if (btnEnter!=0)
	{
		//write to memory
		EE_WriteVariable(VirtAddVarTab[2], magnets);
		Saved();
	}
	
	//tens position
	if (state == 31)
	{
		if (btnRight !=0)
		{
			state = 32;
			commandToLCD(LCD_LN2+1);
			return;
		}
		else if (btnDown!=0)
		{
			if(magnets<10)
				magnets = magnets + 90;
			else
				magnets = magnets-10;
		}
		else if (btnUp!=0)
		{
			if(magnets>90)
				magnets = magnets - 90;
			else
				magnets = magnets+10;
		}
		//get current value and write it
		commandToLCD(LCD_LN2);
		sprintf(str,"%02d", magnets);
		WriteString(str);
		commandToLCD(LCD_LN2);
	}
		
	//ones position
	else if (state == 32)
	{
		if (btnLeft !=0)
		{
			state = 31;
			commandToLCD(LCD_LN2);
			return;
		}
		else if (btnDown!=0)
		{
			if ((magnets%10) ==0)
				magnets = magnets + 9;
			else
				magnets = magnets-1;
		}
		else if (btnUp!=0)
		{
			if((magnets%10) ==9)
				magnets = magnets - 9;
			else
				magnets = magnets+1 ;
		}
		//get current value and write it
		commandToLCD(LCD_LN2);
		sprintf(str,"%02d", magnets);
		WriteString(str);
		commandToLCD(LCD_LN2+1);
	}
	
}

/*
* Name:Speed Limit
* Type:PUBLIC
* Parameters:left, right, enter
* Returns:nothing
* Description:This function runs the Speed Limiter sub menu
*/
void SpeedLimit(uint8_t btnRight, uint8_t btnLeft, uint8_t btnEnter)
{
	//if we're saving, we don't need to do any other code
	if (btnEnter!=0)
	{
		//write to memory...somehow.
		Saved();
	}
	
	if (state == 41)
	{
		if (btnRight !=0)
		{
			state = 42;
			speedLimit = 0;
			commandToLCD(LCD_LN2+2);
			WriteString(" ");
			DataToLCD(LCD_RGT);
			commandToLCD(LCD_HID);
		}
	}
	if (state == 42)
	{
		if (btnLeft !=0)
		{
			state = 41;
			speedLimit = 1;
			commandToLCD(LCD_LN2+2);
			DataToLCD(LCD_LFT);
			WriteString(" ");
			commandToLCD(LCD_HID);
		}
	}
}

/*
* Name:MainScreen
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:This function writes the main information to the screen
*/
void MainScreen(void)
{
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_LN1);
	WriteString("Speed: 99km/h");
	commandToLCD(LCD_LN2);
	WriteString("Battery: 110%");
	commandToLCD(LCD_HID);
}

/*
* Name:MainSize
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:This function writes the wheel size menu option to the screen
*/
void MainSize(void)
{
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_LN1);
	WriteString("Wheel Size");
	commandToLCD(LCD_LN2);
	WriteString("Setup");
	commandToLCD(LCD_HID);
}

/*
* Name:MainSensor
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:This function writes the wheel size menu option to the screen
*/
void MainSensor(void)
{
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_LN1);
	WriteString("Wheel Sensor");
	commandToLCD(LCD_LN2);
	WriteString("Setup");
	commandToLCD(LCD_HID);
}

/*
* Name:MainSensor
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:This function writes the speed limiter menu option to the screen
*/
void MainLimit(void)
{
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_LN1);
	WriteString("Speed Limiter");
	commandToLCD(LCD_HID);
}

/*
* Name:SizeSetup
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:This function sets up the wheel size data screen
*/
void SizeSetup(void)
{
	char str[3];
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_LN1);
	WriteString("Wheel Radius");
	commandToLCD(LCD_LN2);
	//get value from memory and write it
	EE_ReadVariable(VirtAddVarTab[1], &wheelSize);
	sprintf(str,"%02d", wheelSize);
	WriteString(str);
	WriteString("cm");
	commandToLCD(LCD_LN2);  
	state = 21;
}

/*
* Name:SensorSetup
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:This function sets up the wheel sensor data screen
*/
void SensorSetup(void)
{
	char str[3];
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_LN1);
	WriteString("# of Magnets");
	commandToLCD(LCD_LN2);
	//get value from memory and  and write it here
	EE_ReadVariable(VirtAddVarTab[2], &magnets);
	sprintf(str,"%02d", magnets);
	WriteString(str);
	commandToLCD(LCD_LN2);
	state = 31;
}

/*
* Name:LimiterSetup
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:This function sets up the speed limiter data screen
*/
void LimiterSetup(void)
{
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_LN1);
	WriteString("Speed Limiter");
	commandToLCD(LCD_LN2);
	WriteString("ON  OFF");
	commandToLCD(LCD_HID);
	//read from memory?, and set state to 41 or 42 depending on value
	//speedLimit ==
	
	if (speedLimit == 1)
	{
		state = 41;
		commandToLCD(LCD_LN2+2);
		DataToLCD(LCD_LFT);
		commandToLCD(LCD_HID);
	}
	else
	{
		state = 42;
		commandToLCD(LCD_LN2+3);
		DataToLCD(LCD_RGT);
		commandToLCD(LCD_HID);
	}
}

/*
* Name:Saved
* Type:PUBLIC
* Parameters:nothing
* Returns:nothing
* Description:This function shows "Saved!" on the screen for a second 
**	then returns to the main menu.  to be called after saving a value to memory
*/
void Saved()
{
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_LN1);
	WriteString("Saved!");
	delay(12000000);
	state = 1;
	MainScreen();
}
