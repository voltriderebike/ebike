/* Name: main.c
*/


#include "clocks_lib.h"
#include "fan_control.h"
#include "stm32f10x.h"
#include "LCD_lib.h"
#include "SPI_lib.h"
#include "GUI_lib.h"
#include "stm32f10x.h"
#include "eeprom.h"

//Define Global Variables
int channel=12;     			//defines the channel (what adc to use).  Starts with channel 13 by default
int radius=45;     			  //diameter in cm
int magnetCount=2;  			//number of magnets attached to wheel
int rearSpeed=0;			    //rear tire speed in Km/h
int startTimeRear=0;      //current time before hall effect pulse
int endTimeRear=0;        //current time after hall effect pulse
int highPulse=0;          //keeps track of the number of high hall effect pulses
int lowPulse=0;						//keeps track of the number of low hall effect pulses
int charge=0;							//percent of battery charge

int main (void) 
{
	
	uint8_t switchData = 0x30;
	uint8_t btnUp, btnDown, btnLeft, btnRight, btnEnter;
	clockInit();
	a2d_init();
	pwmInit();
	NVIC_EnableIRQ(ADC1_IRQn);   //enable NVIC
	ADC1->CR2 |= ADC_CR2_ADON;   //start conversion
	
	// SCREEN CODE //
	//sw3:up sw1:right sw4:left sw2:down sw5:enter
	
	//Functions to execute once	
	clockInit();
	SPIInit();
	//testing
	/*
	WriteSPICommand(0x00);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	WriteSPICommand(0x05);
	
	WriteSPICommand(0x01);
  WriteSPICommand(0x02);
	WriteSPICommand(0x03);
	WriteSPICommand(0x04);
	WriteSPICommand(0x05);
	WriteSPICommand(0x06);
	WriteSPICommand(0x07);
	WriteSPICommand(0x08);
	*/
	LCDInit();//needs to be after SPIInit() since it uses SPI communication
	
	//enable eeprom emulation
	FLASH_Unlock();
  EE_Init();
	
	
	//setup main display	
	MainScreen();
	
	//Main Loop (functions to execute continuously)
	while (1)
	{
		//top three bits are garbage.  data from 5 switches is in lowest 5 bits.
		switchData = ReadSPI();
		switchData = switchData&0x1F;
		while (switchData==0)
		{
			switchData = ReadSPI();
			switchData = switchData&0x7C;//0x1F;
			UpdateSpeed();//testing periodic speed update here.  may need to be moved
		}
		delay(60000);//debouncing
		
		/*
		btnRight = switchData & 0x01;
		btnDown = (switchData & 0x02) >> 1;
		btnUp = (switchData & 0x4) >> 2;
		btnLeft = (switchData & 0x8) >> 3;
		btnEnter = (switchData & 0x10) >> 4;
		*/
		btnRight = (switchData & 0x08) >> 3;
		btnDown = (switchData & 0x04) >> 2;
		btnUp = (switchData & 0x20) >> 5;
		btnLeft = (switchData & 0x10) >> 4;
		btnEnter = (switchData & 0x40) >> 6;
		
		RunGUI(btnRight, btnLeft, btnUp, btnDown, btnEnter);
		
		while (switchData!=0)
		{
			switchData = ReadSPI();
			switchData = switchData&0x7C;//0x1F;
		}
		delay(60000);//debouncing
	}
		return 0;
}


//Interrupt handler
void ADC1_IRQHandler(void)
{
	int elapsedTimeRear=0;		 //time in ms*10 (time between hall effect pulses)
	float x=0;                 //value to be converted to mV
	float value=0;             //raw data value
	float actualVoltage = 0;   //voltage held by battery
	
	// PWM
	if (channel == 12) 
	{
		value=ADC1->DR;
	  x=(value*0.8*0.9);  //convert to mV 
		
	  if(x<1000 || rearSpeed>32)
	  {
		  TIM2->CCR2=0; // 0 pulse width when user is not pressing throttle or speed is over 32 km/h (for safety)
	  }
	  else
	  {
	  	TIM2->CCR2=(x/30);  //change pulse width according to throttle (min=850 mV, max=2948 mV)
	  }

		//select next channel (rear wheel)
		ADC1->SQR3 = 11;
	  channel = 11;
	 	ADC1->CR2 |= ADC_CR2_ADON;   //start conversion
	}
	
	// REAR WHEEL SENSOR
	else if (channel == 11)
	{
		  startTimeRear=endTimeRear;
		  value=ADC1->DR;
			x=(value*0.8*0.9);  //convert to mV	
	
		  if(x<1000)
			{
				lowPulse=1;
				highPulse=0;
			}
			else if(x>1050)					
			{
				highPulse=1;
			}
			if(highPulse==1 && lowPulse==1)							//if pulse is sensed, update elapsed time
			{
				endTimeRear=TIM1->CNT;
				if(startTimeRear> endTimeRear)
				{
					elapsedTimeRear = (endTimeRear)-(startTimeRear)+60000;		//corrects for timer loop-around
				}
				else
				{
					elapsedTimeRear = (endTimeRear)-(startTimeRear);
				}
				rearSpeed = (((6.28318*radius)/(elapsedTimeRear/10))/magnetCount)*36; //(circumference/time/number of magnets)*km/hour conversion number
				lowPulse=0;		//reset low pulse value
				highPulse=0;  //resent high pulse value
			}
			
			//select next channel (battery voltage)
				ADC1->SQR3 = 14;
				channel = 14;
				ADC1->CR2 |= ADC_CR2_ADON;	//start conversion
		}
	
		
		// BATTERY VOLTAGE
		else if(channel==14)
		{
			value=ADC1->DR;
	    x=(value*0.8*0.9);  			//convert to mV 
			actualVoltage = x/0.254;  //actual battery voltage (using a voltage divider)
			charge = ((actualVoltage-9000)/5800)*100;  //9V=0%, 14.8V=100%
			
			//select next channel (PWM)
			ADC1->SQR3 = 12;
			channel = 12;
			ADC1->CR2 |= ADC_CR2_ADON;	//start conversion
		}
}


