/******************************************************************************
 * Name:    LCD_lib.h
 * Description: STM32 peripherals initialization
 * Version: V1.00
 * Authors: Roger Boutin.  
 *
 * 
 *
 *
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *****************************************************************************/
#include <stdint.h>



 //Functions available for public use
void WriteNibble (uint8_t data, uint8_t RS);
void LCDInit (void);
void DataToLCD(uint8_t);
void commandToLCD(uint8_t data);
void WriteString(char *);
